import ballerina/http;
import ballerina/io;

listener http:Listener ep0 = new (9090, config = {host: "localhost"});
final json OriginalJson =
{
    "learner_name": " ",
    "fiestname": " ",
    "lastname": " ",
    "prefered_formats": ["audio", "video", "text"],
        "past_subjects": [{"course": "Algorithms","score": "B+"}, {"course": "Programming I","score": "A+"}]
};

# Description
#
# + learner_id - Field Description  
# + learner_name - Field Description  
# + lastname - Field Description  
# + firstname - Field Description  
# + prefered_formats - Field Description  
# + past_subjects - Field Description  
type userDetails record {|
    int learner_id;
    string learner_name;
    string lastname;
    string firstname;
    string new_learner;
    any[] prefered_formats;
    any[] past_subjects;
|};

type Module record {|
    string course;
    string score;
|};

type new_info userDetails|string;

userDetails[] learners = [];

service /virtualEnvironment on new http:Listener(9090) {


    # Description
    #
    # + new_learner - Parameter Description
    # + return - Return Value Description  
    resource function POST new_learner(@http:Payload userDetails new_learner) returns application/json {

        io:println("Processing the information....");
        learners.push(new_learner);
        return {"New learner has been created at "+new_learner};
    }

    resource function GET getLearners() returns userDetails[] {
        io:println("Retreiving users....");
        return learners;
    }

    # Description
    #
    # + learner_id - Parameter Description
    # + return  - Return Value Description  
    resource function GET get_learner(int learner_id) returns userDetails[]{
        io:println("Retreiving users....");
    }

    resource function PUT update_learner/[int Id](@http:Payload userDetails newInfo) returns json {
        string firstName = newInfo.firstname;
        string lastName = newInfo.lastname;
        string learner_name = newInfo.lastname;
        any[] formats = newInfo.prefered_formats;
        any[] modules = newInfo.past_subjects;

        learners[Id].firstname = firstName;
        learners[Id].lastname = lastName;
        learners[Id].learner_name = learner_name;
        learners[Id].prefered_formats = formats;
    }
}